# cern-yum-tool

cern-yum-tool is a CERN utility which provides the ability to modify the /etc/yum/vars/cernbase content which is used by all 'CentOS' .repo files when used at CERN.

This allows a user to configure their system to use 'production' (week delay of updates), 'testing' (bleeding edge), or set a custom delay based on a number of days. 

Currently this tool is only supported on CentOS8
