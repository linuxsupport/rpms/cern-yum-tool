SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})

clean:
	rm -rf build/ *.tgz

sources:
	tar -zcvf $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^src/,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src/*

rpm: sources
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/src' $(SPECFILE)
