%{!?dist: %define dist .el8.cern}
Name: cern-yum-tool
Version: 1.9
Release: 1%{dist}
Summary: Utility to modify .repo files at CERN
Group: Applications/System
Source0: cern-yum-tool
License: GPLv3
Vendor: CERN
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch
Requires: python3-requests
Provides: cern-dnf-tool

%description
A CERN utility which provides the ability to modify the
/etc/yum/vars/{cernbase,cernstream8} content which is used by all 'CentOS' .repo
files when used at CERN. This allows a user to configure their system
to use 'production' (week delay of updates), 'testing' (bleeding edge), or
set a custom delay based on a number of days.

This tool is currently only supported with CentOS Linux 8 and CentOS Stream 8.

%prep

%build


%install
mkdir -p $RPM_BUILD_ROOT/usr/sbin
install -m 0755 %SOURCE0 $RPM_BUILD_ROOT/usr/sbin
ln -sf /usr/sbin/cern-yum-tool $RPM_BUILD_ROOT/usr/sbin/cern-dnf-tool

%files
%defattr(-,root,root)
%{_sbindir}/cern-yum-tool
%{_sbindir}/cern-dnf-tool

%changelog
* Tue Nov 29 2022 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.9-1
- add support for Red Hat and AlmaLinux

* Wed Nov 03 2021 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.8-1
- add support for CentOS Stream 9

* Thu Feb 18 2021 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.7-1
- add support for CentOS Stream 8

* Thu Jul 30 2020 Ben Morrice <ben.morrice@cern.ch> - 1.6-1
- add support to use 'prerelease' url ...

* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 1.5-2
- fix logic error with disclaimer message

* Mon Jan 20 2020 Ben Morrice <ben.morrice@cern.ch> - 1.5-1
- add disclaimer message when using -r

* Mon Jan 20 2020 Ben Morrice <ben.morrice@cern.ch> - 1.4-2
- define Requires of python3-requests

* Fri Jan 17 2020 Ben Morrice <ben.morrice@cern.ch> - 1.4-1
- add ability to configure repo baseurl via YYYYMMDD format
- extend repo validation checks
- general code cleanup

* Wed Jan 08 2020 Ben Morrice <ben.morrice@cern.ch> - 1.3-1
- add ability to configure 8.x minor releases

* Mon Jan 06 2020 Ben Morrice <ben.morrice@cern.ch> - 1.2-2
- alias cern-dnf-tool to cern-yum-tool

* Fri Nov 22 2019 Ben Morrice <ben.morrice@cern.ch> - 1.2-1
- remove hard python3 dependency

* Tue Nov 12 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1-1
- add extra error handling for when running as a non root user

* Tue Oct 15 2019 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
- Initial release
